const pb = new PocketBase('https://pocketbase.bitt.app');


const previewRoot = document.getElementById("preview-root")

function buildPreviewCard(ownerName, avatar, previewImage, catName, galleryId) {
  const newElement = document.createElement("div")
  newElement.innerHTML = "<div class=\"uk-card uk-margin uk-card-default \">\n" +
    "      <div class=\"uk-card-header\">\n" +
    "        <div class=\"uk-grid-small uk-flex-middle\" uk-grid>\n" +
    "          <div class=\"uk-width-auto\">\n" +
    "            <img class=\"uk-border-circle\" width=\"40\" height=\"40\" src=\""+ avatar +"\" alt=\"Avatar\">\n" +
    "          </div>\n" +
    "          <div class=\"uk-width-expand\">\n" +
    "            <h3 class=\"uk-card-title uk-margin-remove-bottom\">"+ownerName+"</h3>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class='uk-inline uk-card-body uk-background-cover uk-height-large uk-width-large uk-flex uk-flex-center uk-flex-middle' data-src='"+previewImage+"' uk-img=\"loading: eager\"><div class='uk-overlay uk-overlay-default uk-position-bottom uk-heading-small'>"+catName+"</div></div>\n" +
    "      <div class=\"uk-card-footer\">\n" +
    "        <a href=\"gallery.html?id="+galleryId+"\" class=\"uk-button uk-button-text\">Открыть Галерею</a>\n" +
    "      </div>\n" +
    "    </div>"
  return newElement
}

async function getFileUrl(item, filename) {
  return await pb.files.getUrl(item, filename)
}


(async () => {
  await pb.collection('cats').getList(1, 50, {
    expand: "owner"
  }).then( async (items) => {
    items.items.map( async (item) => {
      previewRoot.appendChild(buildPreviewCard(
        item.expand.owner.name,
        await getFileUrl(item.expand.owner, item.expand.owner.avatar),
        await getFileUrl(item, item.image[0]),
        item.name,
        item.id
      ))
    })
  })
})()


