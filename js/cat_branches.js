const pb = new PocketBase('https://pocketbase.bitt.app');

const mountRoot = document.getElementById("branches-root");

async function getFileUrl(item, filename) {
  return await pb.files.getUrl(item, filename)
}

function buildItem(imageLink, title) {
  const element = document.createElement("div")
  element.innerHTML = "<div class=\"uk-inline\">\n" +
    "      <img src=\""+imageLink+"\" width=\"450\" alt=\"\">\n" +
    "      <div class=\"uk-overlay uk-overlay-default uk-position-bottom\">\n" +
    "        <p class='uk-heading-small'>"+title+"</p>\n" +
    "      </div>\n" +
    "    </div>"
  return element
}

(
  async () => {
    await pb.collection('cats_branch').getFullList({
      sort: '-created',
    }).then((items) => {
      items.map(async (item) => {
        mountRoot.appendChild(buildItem(
          await getFileUrl(item, item.image),
          item.name
        ))
      })
    })
  }
)();
