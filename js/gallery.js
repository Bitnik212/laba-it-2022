const pb = new PocketBase('https://pocketbase.bitt.app');

async function getFileUrl(item, filename) {
  return await pb.files.getUrl(item, filename)
}

const params = location.search.replace("?", "").split("&");

const galleryId = params[0].split("=")[1];

const mountRoot = document.getElementById("gallery-root");

function buildItem(link) {
  const newElement = document.createElement("div")
  newElement.innerHTML = "    <div class=\"uk-inline uk-margin\">\n" +
    "      <img src=\""+link+"\" width=\"1800\" alt=\"\">\n" +
    "      <div class=\"uk-overlay uk-light uk-position-bottom\">\n" +
    "      </div>\n" +
    "    </div>"
  return newElement
}

(async () => {
  await pb.collection('cats').getOne(galleryId, {
    expand: "owner"
  }).then( async (item) => {
    console.log(item)
    document.getElementById("gallery-name").innerText = "Галерея питомца \""+item.name+"\""
    item.image.map(async (image) => {
      mountRoot.appendChild(buildItem(await getFileUrl(item, image)))
    })
  })
})()
